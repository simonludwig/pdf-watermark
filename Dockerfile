FROM finizco/nginx-node:latest
WORKDIR /usr/src/app
COPY yarn.lock ./yarn.lock
COPY package*.json ./
RUN yarn install
COPY . .
RUN yarn build
EXPOSE 3000
CMD [ "yarn", "start" ]