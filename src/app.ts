import express from "express";
import {overlay} from "./merger";
import {Application} from "express";
import {combinePDF} from "./concat";

export const app:Application = express();

app.get("/api/overlay.pdf", async (req, res) => {
    console.log("Add Watermark to PDF");
    overlay(req.query.cover as string, req.query.url as string, req.query.count==="true").then(stream => {
        console.log("Sending PDF to client");
        res.setHeader('Content-disposition', `inline; filename="${req.query.fileName || "File.pdf"}"`);
        res.setHeader('Content-type', 'application/pdf');
        res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.setHeader('Expires', '-1');
        res.setHeader('Pragma', 'no-cache');
        stream.pipe(res);
    });
});

app.get("/api/concat.pdf", async (req, res) => {
    console.log("Concat URL");
    combinePDF([req.query.cover as string, req.query.url as string]).then(stream => {
        console.log("Sending PDF to client");
        res.setHeader('Content-disposition', `inline; filename="${req.query.fileName || "File.pdf"}"`);
        res.setHeader('Content-type', 'application/pdf');
        res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.setHeader('Expires', '-1');
        res.setHeader('Pragma', 'no-cache');
        stream.pipe(res);
    });
});
