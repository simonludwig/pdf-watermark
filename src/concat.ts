import {downloadFile, overlayFile} from "./merger";
import * as fs from "fs";

const HummusRecipe = require('hummus-recipe');

export const combinePDF = async (files: string[]) => {
    const downloaded = await Promise.all(files.map(file => downloadFile(file)));
    try {
        const recipe = new HummusRecipe(downloaded[0], "output.pdf");
        for (let i = 1; i < downloaded.length; i++) {
            recipe
                .appendPage(`${downloaded[i]}`);
        }
        recipe.endPDF();
    } catch (error) {
        throw error;
    }

    return fs.createReadStream('./output.pdf')
};