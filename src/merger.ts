import {Stream} from "stream";
const fs = require('fs');
const download = require('download');
const HummusRecipe = require('hummus-recipe');


export function overlay(cover: string,pdf: string, count: boolean): Promise<Stream> {
    console.log("Overlay Letterhead over PDF " + pdf + cover);
    return Promise.all([downloadFile(pdf),downloadFile(cover)])
        .then((pdfFile: any[]) => {
            console.log("Merged");
            return fs.createReadStream(overlayFile(pdfFile[0],pdfFile[1], count))
        })
}

export function overlayFile(file: any, cover: any, count: boolean): string {
    console.log(`Overlay ${file} with letterhead`);
    const pdfDoc = new HummusRecipe(file, 'output.pdf');
    for (let i = 0; i < pdfDoc.metadata.pages; i++) {
        console.log("Overlay page " + (i + 1));
        const realPageCount = i + 1;

        const temp = pdfDoc
            .editPage((i + 1));

        temp.image(cover, 0, 0, {width: 600, keepAspectRatio: true});


        if (i >= 0 && count) {
            let pos = 460;

            if (i >= 1 && i % 2 === 0) {
                temp.image('./rechts.png', 0, 0, {width: 600, opacity: 0.5, keepAspectRatio: true})
                pos = 35;
            } else if (i >= 1 && i % 2 === 1) {
                temp.image('./left.png', 0, 0, {width: 600, opacity: 0.5, keepAspectRatio: true})
            }

            temp.text(`videobakers.de | Seite ${realPageCount}`, pos, 800, {
                color: "#067FA7",
                fontSize: 10,
                font: 'Helvetica'
            })
        }

        temp.endPage();

    }

    pdfDoc.endPDF();

    new HummusRecipe('./output.pdf', './dist/output.pdf')
        .endPDF();

    return './dist/output.pdf';
}

export function downloadFile(url: string): Promise<string> {
    console.log(`Downloading ${url}`);
    const fileName = Math.random() + ".pdf";
    return download(url, 'dist', {filename: fileName})
        .then(() => "./dist/" + fileName)
}
