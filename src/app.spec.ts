import request from 'supertest';
import {app} from './app';
import {expect} from 'chai';

describe('Test PingController', () => {

    const FILENAME_HEADER = "content-disposition";

    it('test combine with filename', async () => {
        const pdf1 = "http://www.africau.edu/images/default/sample.pdf";
        const pdf2 = "http://www.africau.edu/images/default/sample.pdf";
        const result = await request(app).get(`/api/concat.pdf?cover=${pdf1}&url=${pdf2}`).send();
        expect(result.headers[FILENAME_HEADER]).eq("inline; filename=\"File.pdf\"");
    }).timeout(10000);

    it('test convert without filename', async () => {
        const cover = "https://videobakers.de/ci/letterhead/basic.png";
        const pdf = "http://www.africau.edu/images/default/sample.pdf";
        const result = await request(app).get(`/api/overlay.pdf?cover=${cover}&url=${pdf}`).send();
        expect(result.headers[FILENAME_HEADER]).eq("inline; filename=\"File.pdf\"");
    }).timeout(10000);

    it('test convert with count', async () => {
        const cover = "https://res.cloudinary.com/hvioxpubt/image/upload/v1631534738/cafonfhl0ncwq6bik0xy.png";
        const pdf = "https://res.cloudinary.com/hvioxpubt/image/upload/v1631534684/y3jekybrpy0yen7tbcdx.pdf";
        const result = await request(app).get(`/api/overlay.pdf?cover=${cover}&url=${pdf}&count=true`).send();
        expect(result.headers[FILENAME_HEADER]).eq("inline; filename=\"File.pdf\"");
    }).timeout(10000);

    it('test convert with filename', async () => {
        const cover = "https://videobakers.de/ci/letterhead/basic.png";
        const pdf = "http://www.africau.edu/images/default/sample.pdf";
        const result = await request(app).get(`/api/overlay.pdf?fileName=script.pdf&cover=${cover}&url=${pdf}`).send();
        expect(result.headers[FILENAME_HEADER]).eq("inline; filename=\"script.pdf\"")
    }).timeout(10000);
});
